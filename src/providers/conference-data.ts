
import { Http, Headers } from '@angular/http';
import { UserData } from './user-data';
import { AlertController } from 'ionic-angular';
import { Injectable, NgZone } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { BackgroundGeolocation, AndroidFingerprintAuth, TouchID } from 'ionic-native';
import { Diagnostic } from 'ionic-native';
import { Network } from 'ionic-native';


import 'rxjs/add/operator/filter';

declare var Connection;

@Injectable()
export class ConferenceData {
    onDevice: boolean;
    data: any;
    Notifydata: any;
    attention: any;
    LocationMessage: any;
    ok: any;
    didnt_authenticate: any;
    scan_finger: any;
    enterPIN: any;
    networkConnection = '';
    langs = '';
    public watch: any;
    public lat: number = 0;
    public lng: number = 0;
    Url = 'http://10.10.11.104:600//api/';
    authenticated: Boolean = false;
    constructor(public http: Http,
        public user: UserData,
        public zone: NgZone,
        public alertCtrl: AlertController,
        public translateService: TranslateService) { }

    load() {

        if (this.data) {
            // already loaded data
            return Promise.resolve(this.data);
        }
        if (this.Notifydata) {
            // already loaded data
            return Promise.resolve(this.Notifydata);
        }

        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.
            //let phoneNumber = this.tut.phoneNumber;
            this.getData();
            resolve(this.data);
        });
    }
    load1() {

        return new Promise(resolve => {

            this.getData();
            resolve(this.data);
        });
    }

    loadNotiffications() {
        if (this.Notifydata) {
            // already loaded data
            return Promise.resolve(this.Notifydata);
        }

        // don't have the data yet
        return new Promise(resolve => {
            // We're using Angular Http provider to request the data,
            // then on the response it'll map the JSON data to a parsed JS object.
            // Next we process the data and resolve the promise with the new data.
            //let phoneNumber = this.tut.phoneNumber;
            this.getNotificationData();
            resolve(this.Notifydata);
        });
    }

    getData() {
        let link = this.Url + 'Guardian/GetStudentsByGuardianId';
        let data1 = JSON.stringify({ Id: window.localStorage.getItem('guardianId') });
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')
        this.http.put(link, data1, { headers: headers })
            .subscribe(res => {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                //alert(JSON.stringify(res.json()));
                this.data = this.processData(res.json());
            });
    }

    getNotificationData() {
        let link = this.Url + 'Guardian/GetNotificationsByGuardianId';
        let data1 = JSON.stringify({ Id: window.localStorage.getItem('guardianId') });
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')
        this.http.put(link, data1, { headers: headers })
            .subscribe(res => {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                //alert(JSON.stringify(res.json()));
                this.Notifydata = res.json();
            });
    }
    processData(data) {
        // just some good 'ol JS fun with objects and arrays
        // build up the data by linking speakers to sessions

        data.tracks = [];

        // loop through each day in the schedule
        data.Item1.forEach(day => {
            // loop through each timeline group in the day
            day.StudentList.forEach(stdnt => {
                this.processSession(data, stdnt);
            });
        });

        return data;
    }

    processSession(data, stdnt) {
        // loop through each speaker and load the speaker data
        // using the speaker name as the key
        stdnt.students = [];
        if (stdnt.FirstName) {
            data.Item1.forEach(stdntList => {
                let student = stdntList.StudentList.find(s => s.FirstName === stdntList);
                if (student) {
                    stdnt.students.push(student);
                    stdnt.sessions = stdnt.sessions || [];
                    stdnt.sessions.push(stdnt);
                }
            });
        }

        if (stdnt.tracks) {
            stdnt.tracks.forEach(track => {
                if (data.tracks.indexOf(track) < 0) {
                    data.tracks.push(track);
                }
            });
        }
    }


    getSpeakers() {
        return this.load().then(data => {
            return data.Item1.forEach(stndList => {
                let stnds = stndList.StudentList;

                return stnds;
            });
        });
    }


    getMap() {
        return this.load().then(data => {
            return data.map;
        });
    }
    startTracking() {
        // Background Tracking

        let config = {
            desiredAccuracy: 0,
            stationaryRadius: 20,
            distanceFilter: 10,
            debug: false,
            interval: 10000,
            startOnBoot: true,
            stopOnTerminate: true
        };

        if (!Diagnostic.isLocationAvailable().then(result => { return result })) {
            Diagnostic.switchToLocationSettings();
        }
        else {
            this.translations();

            BackgroundGeolocation.isLocationEnabled().then(confirm => {
                if (confirm === 0) {
                    alert(this.LocationMessage);
                    BackgroundGeolocation.showLocationSettings();
                }
            });

            BackgroundGeolocation.configure((location) => {
                // Run update inside of Angular's zone
                this.zone.run(() => {
                    this.lat = location.latitude;
                    this.lng = location.longitude;
                });

                let link = this.Url + 'Guardian/DistanceCalculate';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
                let data = JSON.stringify({ CurrentLattitude: location.latitude, CurrentLongitude: location.longitude, GuardianId: window.localStorage.getItem('guardianId') });
                let headers = new Headers();
                headers.append('Content-Type', 'application/json');
                this.http.post(link, data, { headers: headers })
                    .subscribe((data) => {
                    }, error => {
                        console.log(error);
                    });

            }, (err) => {

                console.log(err);

            }, config);

            // Turn ON the background-geolocation system.
            BackgroundGeolocation.start();

        }
    }

    stopTracking() {

        console.log('stopTracking');

        BackgroundGeolocation.stop();
        this.watch.unsubscribe();

    }

    fingerPrintAuth() {
        // Finger Print Authentication
        AndroidFingerprintAuth.isAvailable()
            .then((result) => {
                if (result.isAvailable) {
                    // it is available

                    AndroidFingerprintAuth.show({ clientId: "MiSK Schools", clientSecret: "so_encrypted_much_secure_very_secret" })
                        .then(result => {
                            if (result.withFingerprint) {
                                this.zone.run(() => {
                                    this.authenticated = true;

                                    //window.localStorage.setItem('authenticated', this.authenticated);
                                });
                                window.localStorage.setItem('fingerAuth', 'true');
                                console.log('Successfully authenticated with fingerprint!');
                            } else if (result.withPassword) {
                                this.zone.run(() => {
                                    this.authenticated = true;
                                });
                                window.localStorage.setItem('fingerAuth', 'true');
                                console.log('Successfully authenticated with backup password!');
                            } else {
                                this.zone.run(() => {
                                    this.authenticated = false;
                                });
                                let confalert = this.alertCtrl.create({
                                    title: this.attention,
                                    message: this.didnt_authenticate,
                                    buttons: [{
                                        text: this.ok,
                                        role: 'cancel',
                                        handler: () => {
                                            //TODO: Your logic here
                                        }
                                    }]
                                });
                                confalert.present();
                                console.log('Didn\'t authenticate!');
                            }
                        })
                        .catch(error => {
                            this.zone.run(() => {
                                this.authenticated = false;
                            });
                            let confalert = this.alertCtrl.create({
                                title: this.attention,
                                message: this.didnt_authenticate + error,
                                buttons: [{
                                    text: this.ok,
                                    role: 'cancel',
                                    handler: () => {
                                        //TODO: Your logic here
                                    }
                                }]
                            });
                            confalert.present();
                            console.error(error)
                        }
                        );

                } else {
                    this.zone.run(() => {
                        this.authenticated = true;
                    });
                    window.localStorage.setItem('fingerAuth', 'false');
                    // Fingerprint auth isn't available
                }
            })
            .catch(error => {
                this.zone.run(() => {
                    this.authenticated = true;
                });

                console.error(error)
                window.localStorage.setItem('fingerAuth', 'false');
            });
    }

    touchIDAuth() {
        TouchID.isAvailable()
            .then(
            res => {
                TouchID.verifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel(this.scan_finger, this.enterPIN).then(
                    res => {
                        this.zone.run(() => {
                            this.authenticated = true;
                        });
                        window.localStorage.setItem('fingerAuth', 'true');
                        console.log('Ok', res)
                    },
                    err => {
                        this.zone.run(() => {
                            this.authenticated = false;
                        });
                        let confalert = this.alertCtrl.create({
                            title: this.attention,
                            message: this.didnt_authenticate + err,
                            buttons: [{
                                text: this.ok,
                                role: 'cancel'
                            }]
                        });
                        confalert.present();
                        //window.localStorage.setItem('fingerAuth', 'false');

                        console.error('Error', err)
                    }

                );
            },
            err => {
                window.localStorage.setItem('fingerAuth', 'false');
                this.zone.run(() => {
                    this.authenticated = true;
                });
            }
            );
    }

    isOnline(): boolean {

        if (this.onDevice && Network.connection) {
            return Network.connection !== Connection.NONE;
        } else {
            return navigator.onLine;
        }
    }

    isOffline(): boolean {

        if (this.onDevice && Network.connection) {
            return Network.connection === Connection.NONE;
        } else {
            return !navigator.onLine;
        }
    }

    translations() {

        this.translateService.get('ATN_MSG').subscribe(
            value => {
                // value is our translated string
                this.attention = value;
            }
        );

        this.translateService.get('LOCATION_MESSAGE').subscribe(
            value => {
                // value is our translated string
                this.LocationMessage = value;
            }
        );
        this.translateService.get('OK_BTN').subscribe(
            value => {
                // value is our translated string
                this.ok = value;
            }
        );
        this.translateService.get('AUTH_FAIL').subscribe(
            value => {
                // value is our translated string
                this.didnt_authenticate = value;
            }
        );
        this.translateService.get('SCAN_FINGER').subscribe(
            value => {
                // value is our translated string
                this.scan_finger = value;
            }
        );

        this.translateService.get('ENTER_PIN').subscribe(
            value => {
                // value is our translated string
                this.enterPIN = value;
            }
        );
    }

    networkChekcing() {
        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        this.translateService.get('ATTENTION').subscribe(
            value => {
                this.attention = value;
            }
        );
        this.translateService.get('NETWORK_CONNECTION').subscribe(
            value => {
                this.networkConnection = value;
            }
        );


        if (this.isOffline()) {
            let confirmAlert = this.alertCtrl.create({
                title: this.attention,
                message: this.networkConnection,
                buttons: [{
                    text: this.ok,
                    role: 'cancel'
                }]
            });

            confirmAlert.present();
        }
    }

}
