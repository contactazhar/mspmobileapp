import { ViewChild, Component } from '@angular/core';
import { Events, MenuController, Nav, Platform, AlertController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { Splashscreen, StatusBar, Push } from 'ionic-native';
import { Http, Headers } from '@angular/http';

import { ConferenceData } from '../providers/conference-data';
import { TabsPage } from '../pages/tabs/tabs';
import { RegisterPage } from '../pages/Register/Register';
import { UserData } from '../providers/user-data';
import { AboutSchoolPage } from '../pages/AboutSchool/about-school';
import { DisclaimerPage } from '../pages/Disclaimer/disclaimer';
import { ContactUs } from '../pages/ContactUs/contact-us';
import { PrivacyPolicy } from '../pages/PrivacyPolicy/privacy-policy';
import { ReportProblem } from '../pages/ReportProblem/report-Problem';
import { StartPage } from '../pages/StartPage/StartPage';
import { NoLoginPage } from '../pages/no-login/no-login';

export interface PageObj {
    title: string;
    component: any;
    icon: string;
    index?: number;
}

declare let FCMPlugin;
declare let window: any;

@Component({
    selector: 'ion-app',
    templateUrl: 'app.template.html'
})
export class ConferenceApp {

    authenticated: boolean;

    // the root nav is a child of the root app component
    // @ViewChild(Nav) gets a reference to the app's root nav
    @ViewChild(Nav) nav: Nav;
    // List of pages that can be navigated to from the left menu
    // the left menu only works after login
    appPages: PageObj[] = [
        { title: 'Home', component: TabsPage, index: 0, icon: 'home' },
        { title: 'About School', component: AboutSchoolPage, icon: 'cog' },
        { title: 'Contact Us', component: ContactUs, icon: 'cog' },
        { title: 'Privacy Policy', component: PrivacyPolicy, icon: 'cog' },
        { title: 'Disclaimer', component: DisclaimerPage, icon: 'cog' },
        { title: 'Report a Problem', component: ReportProblem, icon: 'cog' },
    ];
    rootPage: any;
    networkConnection = '';
    ok = '';
    attention = '';
    cancel: any;
    exit: any;
    exitMessage: any;
    confirmAlert: any;
    alerts: boolean = false;
    constructor(public events: Events,
        public userData: UserData,
        public menu: MenuController,
        public platform: Platform,
        public confData: ConferenceData,
        public translateService: TranslateService,
        public http: Http,
        public alertCtrl: AlertController) {

        if (!window.localStorage.getItem('guardianId')) {
            this.rootPage = RegisterPage;
        } else if (!window.localStorage.getItem('verification')) {
            this.rootPage = RegisterPage;
        }
        else if (window.localStorage.getItem('verification') === "error") {
            this.rootPage = RegisterPage;
        } else if (window.localStorage.getItem('fingerAuth') === 'true') {
            this.rootPage = NoLoginPage;
        }
        else if (window.localStorage.getItem('fingerAuth') === 'false') {
            this.rootPage = StartPage;
        }
        else {
            this.rootPage = StartPage;
        }

        let userLang = navigator.language.split('-')[0];
        userLang = /(ar|en|fp|id|ur)/gi.test(userLang) ? userLang : 'en' || 'ar' || 'fp' || 'id' || 'ur';

        //this.translate.setDefaultLang('en');
        if (!window.localStorage.getItem('langs')) {
            let browserLang = this.translateService.getBrowserLang();
            this.translateService.use(browserLang.match(/ar|en|fp|id|ur/) ? browserLang : 'en');
            //this.translateService.use('en');
            this.confData.langs = 'en';
        } else {
            this.translateService.use(window.localStorage.getItem('langs'));
            this.confData.langs = window.localStorage.getItem('langs');
        }

        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        platform.ready().then(() => {

            this.confData.networkChekcing();

            this.platform.registerBackButtonAction(() => {
                if (menu.isOpen()) {
                    menu.close();
                } else if (this.alerts === true) {
                    this.dismissAlert();
                } else {
                    this.appClosing();
                }
            });

            StatusBar.styleDefault();
            Splashscreen.hide();

            confData.startTracking();
            this.pushNotification();

            setTimeout(() => {
                confData.stopTracking();
            }, 30 * 60 * 1000);

        });

        // load the conference data
        confData.load();
        confData.loadNotiffications();

    }
    openPage(page: PageObj) {
        // the nav component was found using @ViewChild(Nav)
        // reset the nav to remove previous pages and only have this page
        // we wouldn't want the back button to show in this scenario
        if (page.index) {
            this.nav.setRoot(page.component, { tabIndex: page.index });

        } else {
            this.nav.setRoot(page.component);
        }

        if (page.title === 'Logout') {
            // Give the menu time to close before changing to logged out
            setTimeout(() => {
                this.userData.logout();
            }, 1000);
        }
    }

    pushNotification() {

        // FCMPlugin.getToken(
        //     function (token) {
        //         alert(token);
        //         this.DeviceId1 = token;
        //         window.localStorage.setItem('DeviceId', token);
        //     },
        //     function (err) {
        //         console.log('error retrieving token: ' + err);
        //     }
        // );

        // FCMPlugin.onNotification(
        //     function (data) {
        //         if (data.wasTapped) {
        //             this.confData.getData();
        //             this.confData.getNotificationData();
        //             alert(data.aps.alert.body);

        //             alert(data.aps.alert.body);
        //         } else {
        //             this.confData.getData();
        //             this.confData.getNotificationData();
        //             alert(data.aps.alert.body);
        //         }
        //     },
        //     function (msg) {
        //         console.log('onNotification callback successfully registered: ' + msg);
        //     },
        //     function (err) {
        //         console.log('Error registering onNotification callback: ' + err);
        //     }
        // );

        // let link = this.confData.Url + 'Guardian/UpdateDeviceId';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
        // let data1 = JSON.stringify({ DeviceId: window.localStorage.getItem('DeviceId'), Id: window.localStorage.getItem('guardianId') });
        // let headers = new Headers();
        // this.confData.getData();
        // this.confData.getNotificationData();
        // headers.append('Content-Type', 'application/json')
        // this.http.post(link, data1, { headers: headers })
        //     .subscribe((data) => {
        //         alert("Success");
        //     }, error => {
        //         alert("Failed");
        //         console.log(error);
        //     });


        var push = Push.init({
            android: {
                senderID: "603615685495",
                icon: "icon",
            },
            ios: {
                alert: "true",
                badge: true,
                sound: 'true'
            },
            windows: {}
        });

        push.on('registration', (data) => {

            console.log(data.registrationId);
            window.localStorage.setItem('DeviceId', data.registrationId);
            let link = this.confData.Url + 'Guardian/UpdateDeviceId';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
            let data1 = JSON.stringify({ DeviceId: data.registrationId, Id: window.localStorage.getItem('guardianId') });
            let headers = new Headers();
            headers.append('Content-Type', 'application/json')
            this.http.post(link, data1, { headers: headers })
                .subscribe((data) => {
                }, error => {
                    console.log(error);
                });
        });

        push.on('notification', (data) => {
            console.log('message', data.message);
            this.confData.getData();
            this.confData.getNotificationData();
            if (data.additionalData.foreground) {
                // if application open, show popup
                let confirmAlert = this.alertCtrl.create({
                    title: data.title,
                    message: data.message,
                    buttons: [{
                        text: 'Ok',
                        role: 'cancel'
                    }]
                });
                confirmAlert.present();
            } else {
                //if user NOT using app and push notification comes
                //TODO: Your logic on click of push notification directly
                this.confData.getData();
                this.confData.getNotificationData();
            }
        });

        push.on('error', (e) => {
            alert(e.message);
            console.log(e.message);
        });
    }

    appClosing() {
        this.translateService.get('CANCEL_BTN').subscribe(
            value => {
                this.cancel = value;
            }
        );
        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        this.translateService.get('EXIT1').subscribe(
            value => {
                this.exit = value;
            }
        );
        this.translateService.get('EXIT2').subscribe(
            value => {
                this.exitMessage = value;
            }
        );
        this.confirmAlert = this.alertCtrl.create({
            title: this.exit,
            message: this.exitMessage,
            buttons: [{
                text: this.cancel,
                role: 'cancel',
                handler: () => {
                    this.alerts = false;
                }
            }, {
                text: this.ok,
                handler: () => {
                    this.platform.exitApp();
                }
            }]
        });

        if (this.nav.canGoBack()) {
            // Go back in history
            this.nav.remove(1);
        } else {
            this.confirmAlert.present().then(() => {
                this.alerts = true;
            });
        }
    }

    dismissAlert() {
        this.confirmAlert.dismiss().then(() => {
            this.alerts = false;
        });
    }
}

