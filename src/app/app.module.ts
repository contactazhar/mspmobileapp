import { NgModule } from '@angular/core';
import { TranslateModule, TranslateStaticLoader, TranslateLoader } from 'ng2-translate/ng2-translate';

import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

import { ConferenceApp } from './app.component';

import { SettingsPage } from '../pages/Settings/Settings';
import { TabsPage } from '../pages/tabs/tabs';
import { BrowserModule } from '@angular/platform-browser';
import { RegisterPage } from '../pages/Register/Register';
import { TruncatePipe } from '../pages/Register/Register';
import { AboutSchoolPage } from '../pages/AboutSchool/about-school';
import { DisclaimerPage } from '../pages/Disclaimer/disclaimer';
import { ContactUs } from '../pages/ContactUs/contact-us';
import { PrivacyPolicy } from '../pages/PrivacyPolicy/privacy-policy';
import { ReportProblem } from '../pages/ReportProblem/report-Problem';
import { EnterOTP } from '../pages/EnterOTP/enterOTP';
import { NotificationsPage } from '../pages/Notifications/Notifications';
import { StudentStatusPage } from '../pages/StudentStatus/StudentStatus';
import { StudentDetailsPage } from '../pages/StudentDetails/StudentDetails';
import { MapPage } from '../pages/map/map';
import { StartPage } from '../pages/StartPage/StartPage';
import { ForgotPassword } from '../pages/forgot-password/forgot-password';
import { NoLoginPage } from '../pages/no-login/no-login';
import { ChangePasswordPage } from '../pages/change-password/change-password';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { MyFilterPipe } from '../pages/StudentStatus/StudentStatus';
import { MobileDetailPage } from '../pages/mobile-detail/mobile-detail';


export function translateLang(http: Http) {
  return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}

@NgModule({
  declarations: [
    ConferenceApp,
    SettingsPage,
    TabsPage,
    RegisterPage,
    AboutSchoolPage,
    ContactUs,
    PrivacyPolicy,
    ReportProblem,
    EnterOTP,
    NotificationsPage,
    StudentStatusPage,
    StudentDetailsPage,
    MapPage,
    StartPage,
    MyFilterPipe,
    TruncatePipe,
    MobileDetailPage,
    ForgotPassword,
    NoLoginPage,
    ChangePasswordPage,
    DisclaimerPage
  ],

  imports: [
    BrowserModule,
    IonicModule.forRoot(ConferenceApp),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: translateLang,
      deps: [Http]
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    SettingsPage,
    TabsPage,
    RegisterPage,
    AboutSchoolPage,
    ContactUs,
    PrivacyPolicy,
    ReportProblem,
    EnterOTP,
    NotificationsPage,
    StudentStatusPage,
    StudentDetailsPage,
    MapPage,
    StartPage,
    MobileDetailPage,
    ForgotPassword,
    NoLoginPage,
    ChangePasswordPage,
    DisclaimerPage
    
  ],
  providers: [ConferenceData, UserData, Storage],
})
export class AppModule { }
