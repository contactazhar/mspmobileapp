import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  templateUrl: 'contact-us.html'
})
export class ContactUs {
  show = false;
  constructor(public platform: Platform, public navCtrl: NavController, public menu: MenuController) {
    var lang = window.localStorage.getItem('langs');
    if (lang === 'ar') {
      this.show = true;
    }
    this.platform.registerBackButtonAction(() => {
      if (menu.isOpen()) {
        menu.close();
      } else {
        this.navCtrl.setRoot(TabsPage);
      }

    })
  }
}
