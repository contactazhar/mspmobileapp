import { Component, Pipe } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { MenuController, NavController, AlertController, Platform } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';


import { TabsPage } from '../tabs/tabs';
import { EnterOTP } from '../EnterOTP/enterOTP';

import { ConferenceData } from '../../providers/conference-data';

@Component({
  templateUrl: 'Register.html',
})
export class RegisterPage {
  showSkip = true;
  phoneNumber = '';
  authenticated: any;
  countryCode = '';
  speakers = [];
  disMissed = [];
  pickedUps = [];
  DeviceId: any;
  networkConnection = '';
  ok = '';
  cancel = '';
  attention = '';
  plsWait = '';
  phNotExist = '';
  constructor(public navCtrl: NavController,
    public http: Http,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public platform: Platform, public translateService: TranslateService, public menu: MenuController) {

    this.menu.enable(false);
    platform.ready().then(() => {
      this.translations();

      if (!window.localStorage.getItem('fingerAuth')) {

        if (this.platform.is('android')) {
          this.confData.fingerPrintAuth();
        }
        if (this.platform.is('ios')) {
          this.confData.touchIDAuth();
        }
        if (this.platform.is('windows')) {
          this.confData.authenticated = true;
        }
      } else if (window.localStorage.getItem('fingerAuth') === 'true') {
        if (this.platform.is('android')) {
          this.confData.fingerPrintAuth();
        }
        if (this.platform.is('ios')) {
          this.confData.touchIDAuth();
        }
        if (this.platform.is('windows')) {
          this.confData.authenticated = true;
        }
      } else {
        this.confData.authenticated = true;
      }

      this.confData.networkChekcing();
    });
  }

  startApp() {
    this.navCtrl.push(TabsPage);
  }

  sendDetails() {


    let loader = this.loadingCtrl.create({
      content: this.plsWait,
    });

    loader.present();

    let confirmAlert = this.alertCtrl.create({
      title: this.attention,
      message: this.phNotExist,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });


    let link = this.confData.Url + 'Guardian/DeviceRegistration';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
    //alert(window.localStorage.getItem('DeviceId'));
    let data1 = JSON.stringify({ PhoneNumber: this.phoneNumber, DeviceId: window.localStorage.getItem('DeviceId') });
    let headers = new Headers();
    headers.append('Content-Type', 'application/json')
    this.http.put(link, data1, { headers: headers })
      .subscribe((data) => {
        loader.dismiss();
        if (data.json() === 'PhNoNotExists') {
          confirmAlert.present();
        } else {
          this.menu.enable(true);
          window.localStorage.setItem('guardianId', data.json());
          window.localStorage.setItem('phNumber', this.phoneNumber);
          this.navCtrl.push(EnterOTP);
        }
        //alert(JSON.stringify(data.json()));
      }, error => {
        console.log(error);
      });
  }

  translations() {
    this.translateService.get('NETWORK_CONNECTION').subscribe(
      value => {
        this.networkConnection = value;
      }
    );
    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );
    this.translateService.get('ATTENTION').subscribe(
      value => {
        this.attention = value;
      }
    );
    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    this.translateService.get('PHNOTEXIST').subscribe(
      value => {
        this.phNotExist = value;
      }
    );
  }
}
@Pipe({
  name: 'truncate'
})
export class TruncatePipe {
  transform(value: string, args: string[]): string {
    let limit = args.length > 0 ? parseInt(args[0], 10) : 10;
    let trail = args.length > 1 ? args[1] : '';
    return value.length > limit ? value.substring(0, limit) + trail : value;
  }
}
