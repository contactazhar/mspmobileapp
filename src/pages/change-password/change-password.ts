import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavParams, NavController, LoadingController, AlertController, Platform, MenuController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { TranslateService } from 'ng2-translate/ng2-translate';


declare let $;
@Component({
    templateUrl: 'change-password.html'
})
export class ChangePasswordPage {
    prevPassword = '';
    newPassword = '';
    confPassword = '';
    reporterId: any;
    ok: any;
    content: any;
    passwordUpdated: any;
    prevPwdNotMatched: any;
    confPwdNotMatched: any;
    prevNewPwdMatched: any;
    enterPrevPwd: any;
    enterNewPwd: any;
    enterConfPwd: any;
    networkConnection = '';
    attention = '';
    cancel: any;
    exit: any;
    exitMessage: any;
    confirmAlert: any;
    alerts: boolean = false;
    constructor(public navParams: NavParams,
        public nav: NavController,
        public http: Http,
        public confData: ConferenceData,
        public platform: Platform,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public translateService: TranslateService, 
        public menu: MenuController) {
        platform.ready().then(() => {

            this.confData.networkChekcing();

             this.platform.registerBackButtonAction(() => {
                if (menu.isOpen()) {
                    menu.close();
                } else if (this.alerts === true) {
                    this.dismissAlert();
                } else {
                    this.appClosing();
                }
            });
            $(document).ready(function () {
                $('input[type=password]').keyup(function () {
                    let str = $(this).val()
                    str = str.replace(/\s/g, '')
                    $(this).val(str)
                });
            });

            this.translations();
        });
    }


    showAlert(message) {

        let confalert = this.alertCtrl.create({
            message: message,
            buttons: [{
                text: this.ok,
                role: 'cancel'
            }]
        });

        confalert.present();
    }

    updatePassword() {
        if (this.prevPassword == '') {
            this.showAlert(this.enterPrevPwd);
        }
        else if (this.newPassword == '') {
            this.showAlert(this.enterNewPwd);
        }
        else if (this.confPassword == '') {
            this.showAlert(this.enterConfPwd);
        }
        else if (this.newPassword != this.confPassword) {
            this.showAlert(this.confPwdNotMatched);
        }
        else if (this.prevPassword == this.newPassword) {
            this.showAlert(this.prevNewPwdMatched);
        }
        else {
            this.translateService.get('CONTENT_MESSAGE').subscribe(
                value => {
                    this.content = value;
                }
            );
            let loader = this.loadingCtrl.create({
                content: this.content,
            });

            loader.present();

            this.reporterId = window.localStorage.getItem("guardianId");
            let url = this.confData.Url + 'Account/ChangePassword';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
            let data = JSON.stringify({ GuardianId: this.reporterId, PreviousPassword: this.prevPassword, NewPassword: this.newPassword });
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post(url, data, { headers: headers })
                .subscribe((data) => {
                    if (data.json() === 'Success') {
                        loader.dismiss();
                        this.prevPassword = '';
                        this.newPassword = '';
                        this.confPassword = '';
                        this.showAlert(this.passwordUpdated);
                    } else {
                        loader.dismiss();
                        this.showAlert(this.prevPwdNotMatched);
                    }
                }, error => {
                    console.log(error);

                });
        }

    }

    translations() {
        this.translateService.get('ENT_PREV_PWD').subscribe(
            value => {
                // value is our translated string
                this.enterPrevPwd = value;
            }
        );
        this.translateService.get('ENT_NEW_PWD').subscribe(
            value => {
                // value is our translated string
                this.enterNewPwd = value;
            }
        );
        this.translateService.get('ENT_CONF_PWD').subscribe(
            value => {
                // value is our translated string
                this.enterConfPwd = value;
            }
        );


        this.translateService.get('OK_BTN').subscribe(
            value => {
                // value is our translated string
                this.ok = value;
            }
        );
        this.translateService.get('PWD_UPDATED').subscribe(
            value => {
                // value is our translated string
                this.passwordUpdated = value;
            }
        );
        this.translateService.get('PREV_PWD_NOT_MATCHED').subscribe(
            value => {
                // value is our translated string
                this.prevPwdNotMatched = value;
            }
        );
        this.translateService.get('CONF_PWD_NOT_MATCHED').subscribe(
            value => {
                // value is our translated string
                this.confPwdNotMatched = value;
            }
        );
        this.translateService.get('PREV_PWD_NEWPWD_MATCHED').subscribe(
            value => {
                // value is our translated string
                this.prevNewPwdMatched = value;
            }
        );
    }
   appClosing() {
        this.translateService.get('CANCEL_BTN').subscribe(
            value => {
                this.cancel = value;
            }
        );
        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        this.translateService.get('EXIT1').subscribe(
            value => {
                this.exit = value;
            }
        );
        this.translateService.get('EXIT2').subscribe(
            value => {
                this.exitMessage = value;
            }
        );
        this.confirmAlert = this.alertCtrl.create({
            title: this.exit,
            message: this.exitMessage,
            buttons: [{
                text: this.cancel,
                role: 'cancel',
                handler: () => {
                    this.alerts = false;
                }
            }, {
                text: this.ok,
                handler: () => {
                    this.platform.exitApp();
                }
            }]
        });

        if (this.nav.canGoBack()) {
            // Go back in history
            this.nav.remove(1);
        } else {
            this.confirmAlert.present().then(() => {
                this.alerts = true;
            });
        }
    }

    dismissAlert() {
        this.confirmAlert.dismiss().then(() => {
            this.alerts = false;
        });
    }
}
