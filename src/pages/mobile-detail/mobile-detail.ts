import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavParams, NavController, LoadingController, AlertController, Platform, MenuController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { TranslateService } from 'ng2-translate/ng2-translate';

@Component({
  templateUrl: 'mobile-detail.html'
})
export class MobileDetailPage {
  oldPhoneNumber = '';
  newPhoneNumber = '';
  confPhoneNumber = '';
  oldPhoneNotMatched: any;
  oldPhoneEmpty: any;
  newPhoneEmpty: any;
  confPhoneEmpty: any;
  reporterId: any;
  ok: any;
  content: any;
  phNoNotMatched: any;
  phNoUpdated: any;
  networkConnection = '';
  attention = '';
  cancel: any;
  exit: any;
  exitMessage: any;
  confirmAlert: any;
  alerts: boolean = false;
  constructor(public navParams: NavParams,
    public nav: NavController,
    public http: Http,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translateService: TranslateService,
    public platform: Platform, public menu: MenuController) {
    platform.ready().then(() => {

      this.confData.networkChekcing();
      this.platform.registerBackButtonAction(() => {
        if (menu.isOpen()) {
          menu.close();
        } else if (this.alerts === true) {
          this.dismissAlert();
        } else {
          this.appClosing();
        }
      });
    });
    this.translations();
  }

  UpdatePhoneNumber() {
    this.translateService.get('CONTENT_MESSAGE').subscribe(
      value => {
        // value is our translated string
        this.content = value;
      }
    );
    if (this.oldPhoneNumber === '') {
      this.showAlert(this.oldPhoneEmpty);
    }
    else if (this.newPhoneNumber === '') {
      this.showAlert(this.newPhoneEmpty);
      //loader.dismiss();
    } else if (this.confPhoneNumber === '') {
      this.showAlert(this.confPhoneEmpty);
      //loader.dismiss();
    } else if (this.newPhoneNumber !== this.confPhoneNumber) {
      this.showAlert(this.phNoNotMatched);
      //loader.dismiss();
    } else {

      let loader = this.loadingCtrl.create({
        content: this.content,
      });

      loader.present();

      this.reporterId = window.localStorage.getItem("guardianId");
      let url = this.confData.Url + 'Account/UpdateMobileNumber';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
      let data = JSON.stringify({ GuardianId: this.reporterId, OldPhoneNumber: this.oldPhoneNumber, NewPhoneNumber: this.newPhoneNumber });
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(url, data, { headers: headers })
        .subscribe((data) => {

          if (data.json() === 'PhNoNotMatched') {
            this.showAlert(this.oldPhoneNotMatched);
            loader.dismiss();
          } else {
            loader.dismiss();
            this.showAlert(this.phNoUpdated);
            this.oldPhoneNumber = '';
            this.newPhoneNumber = '';
            this.confPhoneNumber = '';
          }
        }, error => {
          console.log(error);

        });
    }

  }
  translations() {

    this.translateService.get('OK_BTN').subscribe(
      value => {
        // value is our translated string
        this.ok = value;
      }
    );
    this.translateService.get('PHNONOTMATCHED').subscribe(
      value => {
        // value is our translated string
        this.phNoNotMatched = value;
      }
    );
    this.translateService.get('PHNOUPDATED').subscribe(
      value => {
        // value is our translated string
        this.phNoUpdated = value;
      }
    );

    this.translateService.get('OLDPHEMPTY').subscribe(
      value => {
        // value is our translated string
        this.oldPhoneEmpty = value;
      }
    );
    this.translateService.get('NEWPHEMPTY').subscribe(
      value => {
        // value is our translated string
        this.newPhoneEmpty = value;
      }
    );
    this.translateService.get('CONFPHEMPTY').subscribe(
      value => {
        // value is our translated string
        this.confPhoneEmpty = value;
      }
    );
    this.translateService.get('OLDNOTMATCHED').subscribe(
      value => {
        // value is our translated string
        this.oldPhoneNotMatched = value;
      }
    );

  }

  showAlert(message) {



    let confalert = this.alertCtrl.create({
      message: message,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });

    confalert.present();
  }

  appClosing() {
        this.translateService.get('CANCEL_BTN').subscribe(
            value => {
                this.cancel = value;
            }
        );
        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        this.translateService.get('EXIT1').subscribe(
            value => {
                this.exit = value;
            }
        );
        this.translateService.get('EXIT2').subscribe(
            value => {
                this.exitMessage = value;
            }
        );
        this.confirmAlert = this.alertCtrl.create({
            title: this.exit,
            message: this.exitMessage,
            buttons: [{
                text: this.cancel,
                role: 'cancel',
                handler: () => {
                    this.alerts = false;
                }
            }, {
                text: this.ok,
                handler: () => {
                    this.platform.exitApp();
                }
            }]
        });

        if (this.nav.canGoBack()) {
            // Go back in history
            this.nav.remove(1);
        } else {
            this.confirmAlert.present().then(() => {
                this.alerts = true;
            });
        }
    }

    dismissAlert() {
        this.confirmAlert.dismiss().then(() => {
            this.alerts = false;
        });
    }

}
