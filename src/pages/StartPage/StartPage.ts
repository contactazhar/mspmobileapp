import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { NavController, Platform, LoadingController, AlertController, MenuController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';

import { TabsPage } from '../tabs/tabs';
import { ForgotPassword } from '../forgot-password/forgot-password';
import { ConferenceData } from '../../providers/conference-data';

declare let $: any;
@Component({
  templateUrl: 'StartPage.html',
})
export class StartPage {
  phoneNumber = '';
  password = '';
  showSkip = true;
  authenticated: any;
  networkConnection = '';
  ok = '';
  cancel = '';
  attention = '';
  plsWait = '';
  reqPhNumber: '';
  reqPassword: '';
  loginFailed: any;

  constructor(public navCtrl: NavController,
    public confData: ConferenceData,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public translateService: TranslateService, public menu: MenuController, public http: Http) {
    this.menu.enable(false);
    platform.ready().then(() => {

      this.confData.networkChekcing();

      $(document).ready(function () {
        $('input[type=password]').keyup(function () {
          let str = $(this).val()
          str = str.replace(/\s/g, '')
          $(this).val(str)
        });
      });

      this.translations();

      if (!window.localStorage.getItem('fingerAuth')) {

        if (this.platform.is('android')) {
          this.confData.fingerPrintAuth();
        }
        if (this.platform.is('ios')) {
          this.confData.touchIDAuth();
        }
        if (this.platform.is('windows')) {
          this.confData.authenticated = true;
        }
      } else if (window.localStorage.getItem('fingerAuth') === 'true') {
        if (this.platform.is('android')) {
          this.confData.fingerPrintAuth();
        }
        if (this.platform.is('ios')) {
          this.confData.touchIDAuth();
        }
        if (this.platform.is('windows')) {
          this.confData.authenticated = true;
        }
      } else {
        this.confData.authenticated = true;
      }

    });
  }

  startApp() {
    this.translations();

    if (this.confData.isOffline()) {
      let confirmAlert = this.alertCtrl.create({
        title: this.attention,
        message: this.networkConnection,
        buttons: [{
          text: this.ok,
          role: 'cancel'
        }]
      });

      confirmAlert.present();
    }

    //this.presentLoading();

    if (this.phoneNumber == '') {
      this.showAlert(this.reqPhNumber);
    }
    else if (this.password == '') {
      this.showAlert(this.reqPassword);
    }
    else {
      let loader = this.loadingCtrl.create({
        content: this.plsWait,
      });

      loader.present();
      //this.reportProblem.reporterId = window.localStorage.getItem("guardianId");
      let url = this.confData.Url + 'Account/Login';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
      let data1 = JSON.stringify({ PhoneNumber: this.phoneNumber, MobileOTP: this.password });
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(url, data1, { headers: headers })
        .subscribe((data) => {
          loader.dismiss();
          if (data.json() === "Error") {
            this.showAlert(this.loginFailed);
            this.password = '';
          } else {
            this.menu.enable(true);
            this.navCtrl.setRoot(TabsPage);
          }
        }, error => {
          console.log(error);
        });
    }

  }


  showAlert(message) {

    let confalert = this.alertCtrl.create({
      message: message,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });
    confalert.present();
  }

  translations() {

    this.translateService.get('NETWORK_CONNECTION').subscribe(
      value => {
        this.networkConnection = value;
      }
    );
    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );
    this.translateService.get('ATTENTION').subscribe(
      value => {
        this.attention = value;
      }
    );
    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    this.translateService.get('REQ_PHONE').subscribe(
      value => {
        this.reqPhNumber = value;
      }
    );
    this.translateService.get('REQ_PASS').subscribe(
      value => {
        this.reqPassword = value;
      }
    );
    this.translateService.get('LOGIN_FAILED').subscribe(
      value => {
        this.loginFailed = value;
      }
    );

  }
  presentLoading() {

    let loader = this.loadingCtrl.create({
      content: this.plsWait,
      duration: 1500
    });
    loader.present();
  }

  ForgetPassword() {
    this.navCtrl.push(ForgotPassword);
  }
}
