import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AlertController, Platform } from 'ionic-angular';
import { LoadingController, NavController, MenuController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';

import { ConferenceData } from '../../providers/conference-data';
import { TabsPage } from '../tabs/tabs';

@Component({
  templateUrl: 'report-problem.html'
})
export class ReportProblem {
  reportProblem = { name: '', email: '', message: '', reporterId: '' };
  reqName = '';
  reqEmail = '';
  validEmail = '';
  plsWait = '';
  reqMessage = '';
  ok = '';
  cancel = '';
  successMessage = '';
  networkConnection = '';
  attention = '';
  constructor(public http: Http,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public confData: ConferenceData,
    public translateService: TranslateService, public platform: Platform, public navCtrl: NavController, public menu: MenuController) {
    this.platform.registerBackButtonAction(() => {
      if (menu.isOpen()) {
        menu.close();
      } else {
        this.navCtrl.setRoot(TabsPage);
      }

    })
    this.confData.networkChekcing();

  }
  validateEmail(email) {
    let emailID = this.reportProblem.email;
    let atpos = emailID.indexOf("@");
    let dotpos = emailID.lastIndexOf(".");
    if (atpos < 1 || (dotpos - atpos < 2)) {
      return false;
    }
    return true;

  }
  showAlert(message) {

    let confalert = this.alertCtrl.create({
      subTitle: message,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });
    confalert.present();
  }

  sendProblem() {

    this.translations();


    if (this.reportProblem.name == "") {
      this.showAlert(this.reqName);
    }
    else if (this.reportProblem.email == "") {
      this.showAlert(this.reqEmail);
    }
    else if (!this.validateEmail(this.reportProblem.email)) {
      this.showAlert(this.validEmail);
    }
    else if (this.reportProblem.message == "") {
      this.showAlert(this.reqMessage);
    }
    else {
      let loader = this.loadingCtrl.create({
        content: this.plsWait,
      });

      loader.present();
      this.reportProblem.reporterId = window.localStorage.getItem("guardianId");
      let url = this.confData.Url + 'Guardian/AddReportProblem';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
      let data = JSON.stringify(this.reportProblem);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(url, data, { headers: headers })
        .subscribe((data) => {
          loader.dismiss();
          this.reportProblem = { name: '', email: '', message: '', reporterId: '' };
          this.showAlert(this.successMessage);
        }, error => {
          console.log(error);

        });

    }

  }

  translations() {
    this.translateService.get('REQ_NAME').subscribe(
      value => {
        this.reqName = value;
      }
    );
    this.translateService.get('REQ_EMAIL').subscribe(
      value => {
        this.reqEmail = value;
      }
    );
    this.translateService.get('VALID_EMAIL').subscribe(
      value => {
        this.validEmail = value;
      }
    );
    this.translateService.get('COMMENT_MESSAGE').subscribe(
      value => {
        this.reqMessage = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );

    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );

    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    this.translateService.get('COMMENT_SUCCESS_MESSAGE').subscribe(
      value => {
        this.successMessage = value;
      }
    );
  }
}