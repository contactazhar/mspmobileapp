import { Component } from '@angular/core';

import { NavParams, NavController, AlertController, Platform } from 'ionic-angular';
import { SettingsPage } from '../Settings/Settings';
import { MapPage } from '../map/map';
import { StudentStatusPage } from '../StudentStatus/StudentStatus';
import { NotificationsPage } from '../Notifications/Notifications';
import { TranslateService } from 'ng2-translate/ng2-translate';

import { ConferenceData } from '../../providers/conference-data';

@Component({
    templateUrl: 'tabs.html',
})
export class TabsPage {
    // set the root pages for each tab
    notificationsTabRoot: any = NotificationsPage;
    homeTabRoot: any = StudentStatusPage;
    mapTabRoot: any = MapPage;
    setttingsTabRoot: any = SettingsPage;
    mySelectedIndex: number;
    networkConnection = '';
    ok = '';
    attention = '';
    cancel: any;
    exit: any;
    exitMessage: any;
    constructor(navParams: NavParams,
        public nav: NavController,
        public alertCtrl: AlertController,
        public platform: Platform,
        public confData: ConferenceData,
        public translateService: TranslateService) {

        platform.ready().then(() => {
            this.confData.networkChekcing();

            // this.platform.registerBackButtonAction(() => {
            //     this.appClosing();

            // })
        });
        this.mySelectedIndex = navParams.data.tabIndex || 0;
    }
    appClosing() {
        this.translateService.get('OK_BTN').subscribe(
            value => {
                this.ok = value;
            }
        );
        this.translateService.get('CANCEL_BTN').subscribe(
            value => {
                this.cancel = value;
            }
        );
        this.translateService.get('EXIT1').subscribe(
            value => {
                this.exit = value;
            }
        );
        this.translateService.get('EXIT2').subscribe(
            value => {
                this.exitMessage = value;
            }
        );
        let confirmAlert = this.alertCtrl.create({
            title: this.exit,
            message: this.exitMessage,
            buttons: [{
                text: this.cancel,
                role: 'cancel',
            }, {
                text: this.ok,
                handler: () => {
                    this.platform.exitApp();
                }
            }]
        });


        // Is there a page to go back to?
        if (this.nav.canGoBack()) {
            // Go back in history
            this.nav.remove(1);
        } else {
            confirmAlert.present();
        }
    }
}
