import {Component} from '@angular/core';

import {ConferenceData} from '../../providers/conference-data';

import { NavController } from 'ionic-angular';

declare var google: any;

@Component({
  templateUrl: 'map.html'
})
export class MapPage {

  directionsDisplay;
  map;
  directionsService;
  constructor(public confData: ConferenceData, public navCtrl: NavController) {
    this.confData.networkChekcing();
  }
  start(){
    this.confData.startTracking();
  }
  stop(){
    this.confData.stopTracking();
  }
}


