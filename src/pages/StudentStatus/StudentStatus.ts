import { Component, ViewChild } from '@angular/core';

import { AlertController, App, List, ModalController, NavController, NavParams, MenuController, Platform } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';

import { ConferenceData } from '../../providers/conference-data';
import { StudentDetailsPage } from '../StudentDetails/StudentDetails';
import { UserData } from '../../providers/user-data';
import { TabsPage } from '../tabs/tabs';
import { Pipe, PipeTransform } from '@angular/core';


declare var $: any;

/// <reference path="../jquery.d.ts"/>

@Component({
  templateUrl: 'StudentStatus.html',
})
export class StudentStatusPage {
  // the list is a child of the schedule page
  // @ViewChild('scheduleList') gets a reference to the list
  // with the variable #scheduleList, `read: List` tells it to return
  // the List and not a reference to the element

  group = [];
  session: any;
  pickUpStatus: any = 'Pick Up';
  sessionList: any = [];
  sortedSpeakers: any;
  @ViewChild('scheduleList', { read: List }) scheduleList: List;

  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks = [];
  shownSessions: any;
  groups = [];
  speakers = [];
  disMissed = [];
  pickedUps = [];
  currentdate1 = new Date();
  datetime1 = '';
  datetime2 = '';
  attention = '';
  cancel: any;
  exit: any;
  exitMessage: any;
  ok = '';
  alerts: boolean = false;
  confirmAlert: any;
  constructor(
    public alertCtrl: AlertController,
    public app: App,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public confData: ConferenceData,
    public user: UserData,
    public menu: MenuController,
    public platform: Platform,
    public translateService: TranslateService
  ) {

    if (!window.localStorage.getItem('langs')) {
      this.confData.langs = 'en';
    }
    else {
      this.confData.langs = window.localStorage.getItem('langs');
    }

    this.platform.registerBackButtonAction(() => {
      if (menu.isOpen()) {
        menu.close();
      } else if (this.alerts === true) {
        this.dismissAlert();
      } else {
        this.appClosing();
      }
    });

    this.confData.networkChekcing();
    this.menu.enable(true);
    this.session = navParams.data;
    this.confData.load1().then(data => {

      this.confData.getData();
      this.confData.getNotificationData();
    });
  }

  appClosing() {
    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );
    this.translateService.get('EXIT1').subscribe(
      value => {
        this.exit = value;
      }
    );
    this.translateService.get('EXIT2').subscribe(
      value => {
        this.exitMessage = value;
      }
    );
    this.confirmAlert = this.alertCtrl.create({
      title: this.exit,
      message: this.exitMessage,
      buttons: [{
        text: this.cancel,
        role: 'cancel',
        handler: () => {
          this.alerts = false;
        }
      }, {
        text: this.ok,
        handler: () => {
          this.platform.exitApp();
        }
      }]
    });

    // Is there a page to go back to?
    if (this.navCtrl.canGoBack()) {
      // Go back in history
      this.navCtrl.remove(1);
    } else {
      this.confirmAlert.present().then(() => {
        this.alerts = true;
      });
    }
  }

  dismissAlert() {
    this.confirmAlert.dismiss().then(() => {
      this.alerts = false;
    });
  }
  ionViewDidEnter() {
    this.updateSchedule();
    //this.app.setTitle('Schedule');
  }

  ngAfterViewInit() {
    this.updateSchedule();
  }
  startApp() {
    this.navCtrl.push(TabsPage);
  }

  updateSchedule() {
    // Close any open sliding items when the schedule updates
    this.scheduleList && this.scheduleList.closeSlidingItems();
    this.speakers = this.confData.data.Item1;
    //this.disMissed = this.confData.data.Item2;
    //this.pickedUps = this.confData.data.Item3;
    this.datetime1 = this.confData.data.Item2;
    this.datetime2 = this.confData.data.Item3;

  }

  goToSessionDetail(sessionData) {
    // go to the session detail page
    // and pass in the session data
    this.navCtrl.push(StudentDetailsPage, sessionData);
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.confData.load().then(data => {
      this.confData.getData();
      this.confData.getNotificationData();

      this.speakers = data.Item1;
      this.datetime1 = data.Item2;
      this.datetime2 = data.Item3;
      return this.speakers = data.Item1,
        this.datetime1 = data.Item2,
        this.datetime2 = data.Item3;

    })
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);

  }

}
@Pipe({ name: 'myFilter' })
export class MyFilterPipe implements PipeTransform {
  transform(items: any[], args: any[]): any {

    return items.filter(item => item.FirstName.toLowerCase().indexOf(args[0].FirstName.toLowerCase()) !== -1
      || item.LastName.toLowerCase().indexOf(args[0].LastName.toLowerCase()) !== -1
      || item.Grade.toLowerCase().indexOf(args[0].Grade.toLowerCase()) !== -1);
  }
}
