import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { Http, Headers } from '@angular/http';
import { TranslateService } from 'ng2-translate/ng2-translate';

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPassword {
  phoneNumber = '';
  reqPhNumber: any;
  plsWait: any;
  ok: any;
  phNotExist: any;
  passwordReset: any;
  networkConnection = '';
  attention = '';
  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public confData: ConferenceData,
    public http: Http, public translateService: TranslateService) {
    this.confData.networkChekcing();
  }

  sendDetails() {
    this.translations();

    if (this.phoneNumber === '') {
      this.showAlert(this.reqPhNumber);
    }
    else {
      let loader = this.loadingCtrl.create({
        content: this.plsWait,
      });

      loader.present();
      //this.reportProblem.reporterId = window.localStorage.getItem("guardianId");
      let url = this.confData.Url + 'Account/ForgotPassword';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
      let data1 = JSON.stringify({ PhoneNumber: this.phoneNumber });
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(url, data1, { headers: headers })
        .subscribe((data) => {
          loader.dismiss();
          if (data.json() === "PhNoNotExists") {
            this.showAlert(this.phNotExist);
          } else {
            this.phoneNumber = '';
            this.showAlert(this.passwordReset);
            this.navCtrl.remove(1);
          }
        }, error => {
          console.log(error);
        });
    }

  }

  showAlert(message) {

    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    let confalert = this.alertCtrl.create({
      message: message,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });
    confalert.present();
  }

  translations() {
    this.translateService.get('REQ_PHONE').subscribe(
      value => {
        this.reqPhNumber = value;
      }
    );
    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    this.translateService.get('PHNOTEXIST').subscribe(
      value => {
        this.phNotExist = value;
      }
    );
    this.translateService.get('PASS_RESET').subscribe(
      value => {
        this.passwordReset = value;
      }
    );
  }

}
