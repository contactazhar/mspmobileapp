import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { NavController, Platform, LoadingController, AlertController, MenuController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { ForgotPassword } from '../forgot-password/forgot-password';
import { ConferenceData } from '../../providers/conference-data';


@Component({
  templateUrl: 'no-login.html',
})
export class NoLoginPage {
  phoneNumber = '';
  password: any;
  showSkip = true;
  authenticated: any;
  networkConnection = '';
  ok = '';
  cancel = '';
  attention = '';
  plsWait = '';
  reqPhNumber: any;
  reqPassword: any;
  loginFailed: any;
  loader: any;
  constructor(public navCtrl: NavController,
    public confData: ConferenceData,
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public translateService: TranslateService, public menu: MenuController) {
    this.menu.enable(false);
    platform.ready().then(() => {
      this.confData.networkChekcing();
      this.translationAndAuthntications();
    });
  }

  startApp() {
    this.presentLoading();
    this.confData.networkChekcing();
    this.navCtrl.setRoot(TabsPage);
    this.loader.dismiss();
  }
  showAlert(message) {

    let confalert = this.alertCtrl.create({
      message: message,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });
    confalert.present();
  }
  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: this.plsWait,
    });
    this.loader.present();
  }
  ForgetPassword() {
    this.navCtrl.push(ForgotPassword);
  }

  translationAndAuthntications() {
    this.translateService.get('NETWORK_CONNECTION').subscribe(
      value => {
        this.networkConnection = value;
      }
    );
    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );
    this.translateService.get('ATTENTION').subscribe(
      value => {
        this.attention = value;
      }
    );
    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    this.translateService.get('REQ_PHONE').subscribe(
      value => {
        this.reqPhNumber = value;
      }
    );
    this.translateService.get('REQ_PASS').subscribe(
      value => {
        this.reqPassword = value;
      }
    );
    this.translateService.get('LOGIN_FAILED').subscribe(
      value => {
        this.loginFailed = value;
      }
    );

    if (!window.localStorage.getItem('fingerAuth')) {

      if (this.platform.is('android')) {
        this.confData.fingerPrintAuth();
      }
      if (this.platform.is('ios')) {
        this.confData.touchIDAuth();
      }
      if (this.platform.is('windows')) {
        this.confData.authenticated = true;
      }
    } else if (window.localStorage.getItem('fingerAuth') === 'true') {
      if (this.platform.is('android')) {
        this.confData.fingerPrintAuth();
      }
      if (this.platform.is('ios')) {
        this.confData.touchIDAuth();
      }
      if (this.platform.is('windows')) {
        this.confData.authenticated = true;
      }
    } else {
      this.confData.authenticated = true;
    }

  }

}
