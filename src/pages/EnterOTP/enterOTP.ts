import { Component } from '@angular/core';

import { NavController, AlertController, MenuController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';

import { TabsPage } from '../tabs/tabs';
import { Http, Headers } from '@angular/http';
import { LoadingController } from 'ionic-angular';

import { ConferenceData } from '../../providers/conference-data';


@Component({
  templateUrl: 'enterOTP.html',
})
export class EnterOTP {
  MobileOTP = '';
  content: any;
  attention: any;
  otpmsg: any;
  ok: any;
  networkConnection = '';

  constructor(public navCtrl: NavController, public confData: ConferenceData, public http: Http,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, public translateService: TranslateService, public menu: MenuController) {
    this.confData.load1();
    this.menu.enable(false);

    this.confData.networkChekcing();
  }


  startApp() {
    this.translateService.get('CONTENT_MESSAGE').subscribe(
      value => {
        // value is our translated string
        this.content = value;
      }
    );
    let loader = this.loadingCtrl.create({
      content: this.content,
    });

    loader.present();


    this.translateService.get('ATN_MSG').subscribe(
      value => {
        // value is our translated string
        this.attention = value;
      }
    );

    this.translateService.get('OTP_MSG').subscribe(
      value => {
        // value is our translated string
        this.otpmsg = value;
      }
    );

    this.translateService.get('OK_BTN').subscribe(
      value => {
        // value is our translated string
        this.ok = value;
      }
    );

    let confirmAlert = this.alertCtrl.create({
      title: this.attention,
      message: this.otpmsg,
      buttons: [{
        text: this.ok,
        role: 'cancel'
      }]
    });


    let link = this.confData.Url + 'Guardian/VerifyOtp';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
    let data1 = JSON.stringify({ PhoneNumber: window.localStorage.getItem('phNumber'), MobileOTP: this.MobileOTP });
    let headers = new Headers();
    headers.append('Content-Type', 'application/json')
    this.http.post(link, data1, { headers: headers })
      .subscribe((data) => {
        loader.dismiss();
        //alert(JSON.stringify(data.json()));
        if (data.json() === "success") {
          this.navCtrl.setRoot(TabsPage);
          this.menu.enable(true);
        } else {
          confirmAlert.present();
        }
        window.localStorage.setItem('verification', data.json());
      }, error => {
        console.log(error);
      });
  }
}
