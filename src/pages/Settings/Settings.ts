import { Component } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { PopoverController, NavController } from 'ionic-angular';
import { LoadingController, AlertController, Platform } from 'ionic-angular';
import { MobileDetailPage } from '../mobile-detail/mobile-detail';
import { AndroidFingerprintAuth, TouchID } from 'ionic-native';
import { ChangePasswordPage } from '../change-password/change-password';

import { ConferenceData } from '../../providers/conference-data';

@Component({
    templateUrl: 'Settings.html',
})
export class SettingsPage {
    conferenceDate = '2047-05-17';
    alertTitle: any;
    message: any;
    ok: any;
    isAvailable: any = true;
    authAvailable = false;
    constructor(public popoverCtrl: PopoverController,
        public navCtrl: NavController,
        public translate: TranslateService,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController, public translateService: TranslateService,
        public platform: Platform, public confData: ConferenceData) {
        if (!window.localStorage.getItem('fingerAuth')) {
            this.isAvailable = true;
            window.localStorage.setItem('fingerAuth', this.isAvailable);
        } else {
            if (!AndroidFingerprintAuth.isAvailable().then(rest => rest.isAvailable)) {
                this.isAvailable = false;
                window.localStorage.setItem('fingerAuth', this.isAvailable);
            }
            if (!TouchID.isAvailable()) {
                this.isAvailable = false;
                window.localStorage.setItem('fingerAuth', this.isAvailable);
            }
            this.isAvailable = window.localStorage.getItem('fingerAuth');
        }
        this.translate.addLangs(['en', 'ar', 'fp', 'id', 'ur']);
        this.fingerPrintEnable();

    }
    mobileNoChange() {
        //alert('f');
        this.navCtrl.push(MobileDetailPage);
    }
    passwordChange() {
        this.navCtrl.push(ChangePasswordPage);
    }
    langChange() {

        window.localStorage.setItem('fingerAuth', this.isAvailable);
        //alert(this.isAvailable);
        window.localStorage.setItem('langs', this.translate.currentLang);
        
        this.confData.langs = window.localStorage.getItem('langs');

        this.translate.use(window.localStorage.getItem('langs'));

        this.translations()

        let confirmAlert = this.alertCtrl.create({

            title: this.alertTitle,
            message: this.message,
            buttons: [{
                text: this.ok,
            }]
        });
        confirmAlert.present();
    }

    fingerPrintEnable() {
        if (this.platform.is('android')) {
            AndroidFingerprintAuth.isAvailable().then(rest => {
                this.authAvailable = rest.isAvailable;
            })
        }
        if (this.platform.is('ios')) {
            TouchID.isAvailable().then(rest => {
                this.authAvailable = rest.isAvailable;
            })
        }

    }

    translations() {

        this.translateService.get('SUCCESS').subscribe(
            value => {
                // value is our translated stringasdasd
                this.alertTitle = value;
            }
        );
        this.translateService.get('SETTING_SAVE').subscribe(
            value => {
                // value is our translated string
                this.message = value;

            }
        );
        this.translateService.get('OK_BTN').subscribe(
            value => {
                // value is our translated string
                this.ok = value;
            }
        );
    }
}
