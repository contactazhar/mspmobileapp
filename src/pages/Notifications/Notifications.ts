import { Component } from '@angular/core';

import { ActionSheet, ActionSheetController, NavController } from 'ionic-angular';

import { ConferenceData } from '../../providers/conference-data';


@Component({
  templateUrl: 'Notifications.html',
})
export class NotificationsPage {
  actionSheet: ActionSheet;
  speakers = [];
  disMissed = [];
  pickedUps = [];
  segment = 'queued';
  currentdate1 = new Date();
  datetime1 = '';
  datetime2 = '';
  rollBack = '';
  chkRollBack = [];
  constructor(public actionSheetCtrl: ActionSheetController, public navCtrl: NavController, public confData: ConferenceData) {
    if (!window.localStorage.getItem('langs')) {
      this.confData.langs = 'en';
    }
    else {
      this.confData.langs = window.localStorage.getItem('langs');
    }

    this.confData.networkChekcing();
    this.confData.loadNotiffications().then(data => {
      this.confData.getNotificationData();
      this.confData.getData();

      for (let i = 0; i < data.Item1.length; i++) {
        this.rollBack = data.Item1[i].Message;
        var rolback = this.rollBack.includes('sent back');
        this.chkRollBack.push(rolback);
      }


      return this.speakers = data.Item1,
        this.disMissed = data.Item2,
        this.pickedUps = data.Item3;
    });


  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.confData.loadNotiffications().then(data => {
      this.confData.getNotificationData();
      this.confData.getData();

      this.chkRollBack =[];
      
      for (let i = 0; i < data.Item1.length; i++) {
        this.rollBack = data.Item1[i].Message;
        var rolback = this.rollBack.includes('sent back');
        this.chkRollBack.push(rolback);
      }

      return this.speakers = data.Item1,
        this.disMissed = data.Item2,
        this.pickedUps = data.Item3;
    })

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);

  }

}
