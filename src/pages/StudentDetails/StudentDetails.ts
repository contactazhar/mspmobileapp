import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NavParams, NavController, LoadingController, AlertController, Platform, MenuController } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';
import { TranslateService } from 'ng2-translate/ng2-translate';


@Component({
  templateUrl: 'StudentDetails.html'
})
export class StudentDetailsPage {
  session: any;
  showSkip: any = false;
  plsWait: any;
  networkConnection = '';
  attention = '';
  ok: any;
  CurntDate: any;
  cancel: any;
  exit: any;
  exitMessage: any;
  constructor(public navParams: NavParams,
    public nav: NavController,
    public http: Http,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController, public translateService: TranslateService,
    public alertCtrl: AlertController, public platform: Platform, public menu: MenuController) {

    if (!window.localStorage.getItem('langs')) {
      this.confData.langs = 'en';
    }
    else {
      this.confData.langs = window.localStorage.getItem('langs');
    }


    platform.ready().then(() => {
      this.confData.networkChekcing();

      this.platform.registerBackButtonAction(() => {
        if (menu.isOpen()) {
          menu.close();
        }
        this.appClosing();
      })
    });
    this.session = navParams.data;

    var d = new Date();

    this.CurntDate = d.getFullYear() + '-' + (d.getMonth() < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1) + '-' + (d.getDate() < 10 ? "0" + d.getDate() : d.getDate()) + 'T00:00:00';

    if (this.session.Status === 3) {
      if (this.session.CurrentDate === this.CurntDate) {
        this.showSkip = true;
      }
    }
  }
  pickedUp() {
    this.translateService.get('PLSWAIT').subscribe(
      value => {
        this.plsWait = value;
      }
    );
    let loader = this.loadingCtrl.create({
      content: this.plsWait,
    });

    loader.present();
    let link = this.confData.Url + 'Guardian/UpdateStudentPickedStatus';//?CurrentLattitude='+this.lat+'&CurrentLongitude='+this.lng;
    let data1 = JSON.stringify({ Id: this.session.Id, GateName: this.session.GateName, GuardianId: window.localStorage.getItem('guardianId') });
    let headers = new Headers();
    headers.append('Content-Type', 'application/json')
    this.http.post(link, data1, { headers: headers })
      .subscribe((data) => {
        loader.dismiss();
        this.confData.load().then(resolve => {

          this.confData.getData();
          resolve(this.confData.data);
          this.session.Status = 4;
          this.nav.remove(1);
        }, error => {
          console.log(error);
        });
        this.session.Status = 4;

        this.nav.remove(1);
      });

  }

  appClosing() {
    this.translateService.get('OK_BTN').subscribe(
      value => {
        this.ok = value;
      }
    );
    this.translateService.get('CANCEL_BTN').subscribe(
      value => {
        this.cancel = value;
      }
    );
    this.translateService.get('EXIT1').subscribe(
      value => {
        this.exit = value;
      }
    );
    this.translateService.get('EXIT2').subscribe(
      value => {
        this.exitMessage = value;
      }
    );
    let confirmAlert = this.alertCtrl.create({
      title: this.exit,
      message: this.exitMessage,
      buttons: [{
        text: this.cancel,
        role: 'cancel',
      }, {
        text: this.ok,
        handler: () => {
          this.platform.exitApp();
        }
      }]
    });


    // Is there a page to go back to?
    if (this.nav.canGoBack()) {
      // Go back in history
      this.nav.remove(1);
    } else {
      confirmAlert.present();
    }
  }
}
