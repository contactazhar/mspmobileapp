import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

@Component({
  templateUrl: 'privacy-policy.html'
})
export class PrivacyPolicy {
  constructor(public platform: Platform, public navCtrl: NavController, public menu: MenuController) {
    this.platform.registerBackButtonAction(() => {
      if (menu.isOpen()) {
        menu.close();
      } else {
        this.navCtrl.setRoot(TabsPage);
      }

    })
  }
}
